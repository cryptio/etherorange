use libc::{if_indextoname, if_nametoindex};
use std::ffi::{CStr, CString};
use std::io;

/// Chercher l'indice d'interface à partir de son nom
/// Pris de `util` de aya
///
/// # Arguments
///
/// * `if_name` - Le nom de l'interface
pub fn ifindex_from_ifname(if_name: &str) -> Result<u32, io::Error> {
    let c_str_if_name = CString::new(if_name)?;
    let c_if_name = c_str_if_name.as_ptr();
    // Safety: libc wrapper
    let if_index = unsafe { if_nametoindex(c_if_name) };
    if if_index == 0 {
        return Err(io::Error::last_os_error());
    }
    Ok(if_index)
}

/// Chercher le nom d'un interface à partir de son indice
///
/// # Arguments
///
/// * `if_index` - L'indice d'interface à chercher
pub fn ifname_from_ifindex(if_index: u32) -> Result<String, io::Error> {
    let mut buf = Vec::<i8>::with_capacity(16);
    let ret = unsafe { if_indextoname(if_index, buf.as_mut_ptr()) };
    if (ret as *const _) == std::ptr::null() {
        return Err(io::Error::last_os_error());
    }
    // SAFETY: On a déjà vérifié que ret n'est pas nulle
    let res = unsafe { CStr::from_ptr(ret) };
    Ok(res.to_str().unwrap().to_owned())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_ifindex_from_ifname_non_existent_ifname() {
        assert!(ifindex_from_ifname("foo").is_err());
    }

    #[test]
    fn test_ifindex_from_ifname_success() {
        // L"interface lo devrait exister sur tous les systèmes
        let ifindex = ifindex_from_ifname("lo");
        assert!(ifindex.is_ok());
        assert_eq!(ifindex.unwrap(), 1);
    }

    #[test]
    fn test_ifname_from_ifindex_non_existent_ifindex() {
        assert!(ifname_from_ifindex(std::u32::MAX).is_err());
    }

    #[test]
    fn test_ifname_from_ifindex_sucess() {
        let ifname = ifname_from_ifindex(1);
        assert!(ifname.is_ok());
        assert_eq!(ifname.unwrap(), "lo");
    }
}
