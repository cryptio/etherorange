mod util;
use actix_web::{get, web, App, HttpServer, Responder};
use anyhow::Context;
use aya::maps::{Array, MapRefMut, PerCpuHashMap};
use aya::programs::{tc, SchedClassifier, TcAttachType, Xdp, XdpFlags};
use aya::{include_bytes_aligned, Bpf};
use aya_log::BpfLogger;
use clap::Parser;
use etherorange_common::*;
use lazy_static::lazy_static;
use local_ip_address::list_afinet_netifas;
use log::{info, warn};
use prometheus::{self, register_int_counter_vec, Encoder, IntCounterVec, TextEncoder};
use std::convert::TryFrom;
use std::{
    ffi::{CStr, CString},
    io,
    net::IpAddr,
};
use tokio::sync::broadcast;
use tokio::{select, signal};
use util::*;

static mut COMPTEURS_MAP: Option<PerCpuHashMap<MapRefMut, CompteurKey, CompteurVal>> = None;
lazy_static! {
    static ref PAQUETS_COMPTEUR: IntCounterVec = register_int_counter_vec!(
        "paquets",
        "Combien des paquets ont étés réçus",
        &["interface", "protocol"]
    )
    .unwrap();
    static ref OCTETS_COMPTEUR: IntCounterVec = register_int_counter_vec!(
        "octets",
        "Combien des octets ont étés réçus",
        &["interface", "protocol"]
    )
    .unwrap();
}

#[derive(Debug, Parser)]
struct Opt {
    /// Si le programme devrait faire NAT (Network Address Translation) entre
    /// l'interface LAN et WAN
    #[clap(short, long, default_value = "false", requires_all = &["lan_iface", "wan_iface"])]
    utilise_nat: bool,

    /// L'interface à utiliser
    #[clap(short, long, default_value = "eth0", conflicts_with_all = &["lan_iface", "wan_iface"])]
    iface: String,

    /// L'interface qui est connecté au réseau local
    #[clap(short, long, default_value = "lan0", requires = "utilise_nat")]
    lan_iface: String,

    /// L'interface qui est connecté à l'Internet (WAN)
    #[clap(short, long, default_value = "eth0", requires = "utilise_nat")]
    wan_iface: String,

    /// L'adresse IP qui sera utilisé pour le serveur Prometheus
    #[clap(short = 'a', long, default_value = "127.0.0.1")]
    metrics_addr: String,

    /// Le port qui sera utilisé pour le serveur Prometheus
    #[clap(short = 'p', long, default_value = "8080")]
    metrics_port: u16,
}

/// Route Prometheus pour chercher les statistiques
#[get("/metrics")]
async fn metrics() -> impl Responder {
    for compteur in unsafe { COMPTEURS_MAP.as_ref().unwrap().iter() } {
        let (k, percpu_valeurs) = compteur.unwrap();

        let mut compteur_sommaire = CompteurVal {
            paquets: 0,
            octets: 0,
        };
        // Chaque CPU a son propre valeur - additionne-elles afin d'avoir
        // le résultat complète
        for v in percpu_valeurs.iter() {
            compteur_sommaire.paquets += v.paquets;
            compteur_sommaire.octets += v.octets;
        }

        let ifname = ifname_from_ifindex(k.ifindex).unwrap();
        // `IpProto::Udp` -> "udp"
        let proto_key = format!("{:?}", k.proto).to_lowercase();
        PAQUETS_COMPTEUR
            .with_label_values(&[ifname.as_str(), proto_key.as_str()])
            .reset();
        PAQUETS_COMPTEUR
            .with_label_values(&[ifname.as_str(), proto_key.as_str()])
            .inc_by(compteur_sommaire.paquets);
        OCTETS_COMPTEUR
            .with_label_values(&[ifname.as_str(), proto_key.as_str()])
            .reset();
        OCTETS_COMPTEUR
            .with_label_values(&[ifname.as_str(), proto_key.as_str()])
            .inc_by(compteur_sommaire.octets);
    }
    let mut buffer = Vec::<u8>::new();
    let encoder = TextEncoder::new();

    let metric_families = prometheus::gather();
    encoder.encode(&metric_families, &mut buffer).unwrap();

    // Afficher les compteurs dans un format que Prometheus peut comprendre
    format!("{}", String::from_utf8(buffer.clone()).unwrap())
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let opt = Opt::parse();

    env_logger::init();

    // Charger le fichier d'objet eBPF
    #[cfg(debug_assertions)]
    let mut bpf = Bpf::load(include_bytes_aligned!(
        "../../target/bpfel-unknown-none/debug/etherorange"
    ))?;
    #[cfg(not(debug_assertions))]
    let mut bpf = Bpf::load(include_bytes_aligned!(
        "../../target/bpfel-unknown-none/release/etherorange"
    ))?;
    if let Err(e) = BpfLogger::init(&mut bpf) {
        warn!("impossible d'initialiser le logger eBPF: {}", e);
    }

    let intf_primaire: String = if opt.utilise_nat {
        opt.lan_iface.clone()
    } else {
        opt.iface.clone()
    };

    let program_xdp: &mut Xdp = bpf.program_mut("xdp_eorange").unwrap().try_into()?;
    program_xdp.load()?;
    program_xdp.attach(&intf_primaire, XdpFlags::default())
        .context("L'attachement du programme XDP a échoué - essayer de changer XdpFlags::default() à XdpFlags::SKB_MODE")?;

    let _ = tc::qdisc_add_clsact(&intf_primaire);
    let program_tc: &mut SchedClassifier = bpf.program_mut("tc_eorange").unwrap().try_into()?;
    program_tc.load()?;
    program_tc.attach(&intf_primaire, TcAttachType::Ingress)?;

    if opt.utilise_nat {
        let _ = tc::qdisc_add_clsact(&opt.wan_iface);
        program_tc.attach(&opt.wan_iface, TcAttachType::Ingress)?;

        let mut addrs_locaux_map: Array<MapRefMut, AddrLocale> =
            Array::try_from(bpf.map_mut("ADDRS_LOCAUX")?)?;
        let mut addrs6_locaux_map: Array<MapRefMut, Addr6Locale> =
            Array::try_from(bpf.map_mut("ADDRS6_LOCAUX")?)?;

        let network_interfaces = list_afinet_netifas().unwrap();
        for (nom_iface, adresse_ip) in network_interfaces.iter() {
            if *nom_iface != opt.lan_iface && *nom_iface != opt.wan_iface {
                continue;
            }

            let wan = *nom_iface == opt.wan_iface;

            let ifindex = match ifindex_from_ifname(nom_iface) {
                Ok(ifindex) => ifindex,
                Err(_) => {
                    warn!(
                        "Impossible de chercher ifindex pour interface {}",
                        nom_iface
                    );
                    return Ok(());
                }
            };

            match adresse_ip {
                IpAddr::V4(ipv4) => {
                    let padding: [u8; 3] = [0; 3];
                    let addr_locale = AddrLocale {
                        addr: u32::from_be(u32::from(*ipv4)),
                        lan: !wan,
                        padding: padding,
                    };

                    addrs_locaux_map.set(ifindex, addr_locale, 0)?;
                }
                IpAddr::V6(ipv6) => {
                    let padding: [u8; 7] = [0; 7];
                    let addr_locale = Addr6Locale {
                        addr: u128::from_be(u128::from(*ipv6)),
                        lan: !wan,
                        padding: padding,
                    };

                    addrs6_locaux_map.set(ifindex, addr_locale, 0)?;
                }
            }
        }
    }

    unsafe {
        COMPTEURS_MAP = Some(PerCpuHashMap::try_from(bpf.map_mut("COMPTEURS")?)?);
    }

    let serveur_stat = HttpServer::new(|| App::new().service(metrics))
        .bind((opt.metrics_addr, opt.metrics_port))
        .expect("incapable d'initialiser le server HTTP")
        .run();
    tokio::spawn(async move {
        serveur_stat.await;
    });

    info!("En attente pour Ctrl-C...");

    signal::ctrl_c().await?;

    info!("En train de se terminer...");

    Ok(())
}
