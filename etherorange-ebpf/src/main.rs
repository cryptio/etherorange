#![no_std]
#![no_main]
mod forwarded_address;
mod net;
mod packet_context;
mod util;

use aya_bpf::{
    bindings::*,
    helpers::{bpf_fib_lookup, bpf_ktime_get_ns, bpf_redirect},
    macros::{classifier, map, xdp},
    maps::{Array, PerCpuHashMap},
    programs::{TcContext, XdpContext},
};
#[allow(unused_imports)]
use aya_log_ebpf::AYA_LOGS;
use core::mem;
use etherorange_common::*;
use forwarded_address::*;
use net::*;
use network_types::{
    eth::{EthHdr, EtherType},
    ip::{IpProto, Ipv4Hdr, Ipv6Hdr},
};
use packet_context::*;
use util::*;

/// Le nombre d'interfaces qu'on gère, jusqu'à 2 - un pour LAN, l'Autre pour WAN
const NOMBRE_INTERFACES: u32 = 2;

/// Le nombre maximum de compteurs par rapport à combien des champs `IpProto`
/// contient
const COUNTER_NUM: u32 = 148 * NOMBRE_INTERFACES;

/// Map qui compte combien des octets et paquets on a vus
#[map]
static mut COMPTEURS: PerCpuHashMap<CompteurKey, CompteurVal> =
    PerCpuHashMap::<CompteurKey, CompteurVal>::with_max_entries(COUNTER_NUM, 0);

/// Nous faisons l'assomption qu'il n'y a pas d'interface sur le système avec un
/// indice plus grand que ce nombre
const INTERFACES_MAX: u32 = 32;

// Les adresses IPv4 qui sont assignés à la système
#[map]
static mut ADDRS_LOCAUX: Array<AddrLocale> = Array::with_max_entries(INTERFACES_MAX, 0);

// Les adresses IPv6 qui sont assignés à la système
#[map]
static mut ADDRS6_LOCAUX: Array<Addr6Locale> = Array::with_max_entries(INTERFACES_MAX, 0);

/// Fonction d'entrée pour le programme XDP
///
/// # Arguments
///
/// * `ctx` - Le paquet
#[xdp(name = "xdp_eorange")]
pub fn xdp_etherorange(ctx: XdpContext) -> u32 {
    match try_xdp_etherorange(&MyXdpContext(ctx)) {
        Ok(ret) => ret,
        Err(_) => xdp_action::XDP_ABORTED,
    }
}

/// Fonction d'entrée pour le programme TC
///
/// # Arguments
///
/// * `ctx` - Le paquet
#[classifier(name = "tc_eorange")]
pub fn tc_etherorange(ctx: TcContext) -> i32 {
    match try_tc_etherorange(&MyTcContext(ctx)) {
        Ok(ret) => ret,
        Err(_) => TC_ACT_SHOT,
    }
}

/// Fonction de sortie pour les programmes XDP qui enregistre des statistiques
/// sur le trafic
///
/// # Arguments
///
/// * `ip_proto` - Le protocole IP
/// * `action` - L'action XDP à retourner
///
/// # Returns
///
/// La même valeur que le paramètre `action`
#[inline(always)]
fn sortie_xdp(ctx: &MyXdpContext, ip_proto: IpProto, action: u32) -> Result<u32, ()> {
    let data = ctx.data();
    let data_end = ctx.data_end();

    let longeur_donnees = data_end - data;

    let ctr = unsafe { get_or_create_compteur_val(ctx.ingress_ifindex(), ip_proto) };
    if ctr.is_some() {
        let mut unwrapped_ctr = ctr.unwrap();
        unsafe {
            (*unwrapped_ctr).paquets += 1;
            (*unwrapped_ctr).octets += longeur_donnees as u64;
        }
    }

    Ok(action)
}

/// Fonction de sortie pour les programmes TC qui enregistre des statistiques
/// sur le trafic
///
/// # Arguments
///
/// * `data` - Un décalage pour indiquer où se trouve le début du paquet
/// * `data_end` - Un décalage vers le fin du paquet
/// * `ingress_ifindex` - L'indice d'interface d'entrée
/// * `ip_proto` - Le protocole IP
/// * `action` - L'action TC à retourner pour le paquet
///
/// # Returns
///
/// La même valeur que le paramètre `action`
#[inline(always)]
fn sortie_tc(
    data: usize,
    data_end: usize,
    ingress_ifindex: u32,
    ip_proto: IpProto,
    action: i32,
) -> Result<i32, ()> {
    let longeur_donnees = data_end - data;

    let ctr = unsafe { get_or_create_compteur_val(ingress_ifindex, ip_proto) };
    if ctr.is_some() {
        let mut unwrapped_ctr = ctr.unwrap();
        unsafe {
            (*unwrapped_ctr).paquets += 1;
            (*unwrapped_ctr).octets += longeur_donnees as u64;
        }
    }

    Ok(action)
}

/// Créer un `CompteurKey`, l'utiliser pour faire un recherche dans `COUNTERS`
/// et essayer de retourner la valeur correspondant si elle existe
///
/// # Arguments
///
/// * `ifindex` - L'indice d'interface
/// * `ip_proto` - Le protocole IP
///
/// # Returns
///
/// Some() si la recherche dans le map `COMPTEURS` a réussi ou None sinon
#[inline(always)]
unsafe fn get_or_create_compteur_val(ifindex: u32, ip_proto: IpProto) -> Option<*mut CompteurVal> {
    // SAFETY: `Some()` contiendra toujours un pointeur valide, et le validateur
    // empêche l'utilisation des pointeurs nulles
    let mut ctr_key: CompteurKey = unsafe { mem::zeroed() };
    ctr_key.ifindex = ifindex;
    ctr_key.proto = ip_proto;

    let new_compteur_val: CompteurVal = unsafe { mem::zeroed() };
    COMPTEURS.insert(&ctr_key, &new_compteur_val, BPF_NOEXIST as u64);

    COMPTEURS.get_ptr_mut(&ctr_key)
}

/// Retourner un `bpf_fib_lookup` remplit par un entête IPv4 ou IPv6
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `ip_version` - La version IP. `ipv4_hdr` ou `ipv6_hdr` doit être `Some`
///                  en fonction de cette valeur
/// * `ipv4_hdr` - Un pointeur vers l'entête IPv4
/// * `ipv6_hdr` - Un pointeur vers l'entête IPv6
///
/// # Safety
///
/// `ipv4_hdr` ou `ipv6_hdr` ne peut pas être nulle en fonction de
/// `ip_version`
///
///
/// # Returns
///
/// Un object `bpf_fib_lookup`
#[inline(always)]
unsafe fn preparer_parametres_fib(
    ctx: &dyn PacketContext,
    ip_version: &IpVersion,
    ipv4_hdr: &Option<*mut Ipv4Hdr>,
    ipv6_hdr: &Option<*mut Ipv6Hdr>,
) -> bpf_fib_lookup {
    // On doit remplir le contenu de l'objet avec zéros puis assigner
    // les champs un à la fois à cause d'un problème avec `aya`
    let mut fib_params: bpf_fib_lookup = mem::zeroed();
    // SAFETY: ctx ne peut pas être nulle
    fib_params.ifindex = unsafe { ctx.ingress_ifindex() };

    // SAFETY: L'entête ne sera jamais nulle
    if *ip_version == IpVersion::V4 {
        fib_params.family = AddressFamily::AfInet as u8;
        unsafe {
            fib_params.l4_protocol = (*ipv4_hdr.unwrap()).proto as u8;
            fib_params.__bindgen_anon_1.tot_len = u16::from_be((*ipv4_hdr.unwrap()).tot_len);
            fib_params.__bindgen_anon_2.tos = (*ipv4_hdr.unwrap()).tos;
            fib_params.__bindgen_anon_3.ipv4_src = (*ipv4_hdr.unwrap()).src_addr;
            fib_params.__bindgen_anon_4.ipv4_dst = (*ipv4_hdr.unwrap()).dst_addr;
        }
    } else {
        fib_params.family = AddressFamily::AfInet6 as u8;
        unsafe {
            fib_params.l4_protocol = (*ipv6_hdr.unwrap()).next_hdr as u8;
            fib_params.__bindgen_anon_1.tot_len =
                u16::from_be((*ipv6_hdr.unwrap()).payload_len) + mem::size_of::<Ipv6Hdr>() as u16;

            fib_params
                .__bindgen_anon_3
                .ipv6_src
                .copy_from_slice(&(*ipv6_hdr.unwrap()).src_addr.in6_u.u6_addr32);
            fib_params
                .__bindgen_anon_4
                .ipv6_dst
                .copy_from_slice(&(*ipv6_hdr.unwrap()).dst_addr.in6_u.u6_addr32);
        }
    }

    fib_params
}

fn try_tc_etherorange(ctx: &MyTcContext) -> Result<i32, ()> {
    let data = ctx.data();
    let data_end = ctx.data_end();
    let ingress_ifindex = unsafe { ctx.ingress_ifindex() };

    // SAFETY: `ptr_at_mut` pointe toujours vers un endroit de mémoire valide
    // lorsque `Ok()` est retourné
    let eth_hdr: *mut EthHdr = ptr_at_mut(ctx, 0)?;
    let ether_type: EtherType = unsafe { (*eth_hdr).ether_type };

    let from_wan: bool;

    let ip_version: IpVersion;
    match ether_type {
        EtherType::Ipv4 => {
            ip_version = IpVersion::V4;

            // SAFETY: `ctx.ingress_ifindex()` demeure sécuritaire tant que ctx est
            // valide
            if let Some(addr_locale) = unsafe { ADDRS_LOCAUX.get(ctx.ingress_ifindex()) } {
                from_wan = !addr_locale.lan;
            } else {
                return Ok(TC_ACT_OK);
            }
        }
        EtherType::Ipv6 => {
            ip_version = IpVersion::V6;

            // SAFETY: `ctx.ingress_ifindex()` demeure sécuritaire tant que ctx est
            // valide
            if let Some(addr6_locale) = unsafe { ADDRS6_LOCAUX.get(ctx.ingress_ifindex()) } {
                from_wan = !addr6_locale.lan;
            } else {
                return Ok(TC_ACT_OK);
            }
        }
        _ => {
            return Ok(TC_ACT_OK);
        }
    }
    let ip_proto: IpProto;

    let mut ipv4_hdr: Option<*mut Ipv4Hdr> = None;
    let mut ipv6_hdr: Option<*mut Ipv6Hdr> = None;
    // SAFETY: `ptr_at_mut` garantie que les pointeurs sont valides
    unsafe {
        if ip_version == IpVersion::V4 {
            ipv4_hdr = Some(ptr_at_mut(ctx, EthHdr::LEN)?);
            ip_proto = (*ipv4_hdr.unwrap()).proto;
        } else {
            ipv6_hdr = Some(ptr_at_mut(ctx, EthHdr::LEN)?);
            ip_proto = (*ipv6_hdr.unwrap()).next_hdr;
        }
    }

    // La redirection était déjà géré par XDP
    if !from_wan && !cfg!(redirect = "false") {
        return Ok(TC_ACT_OK);
    }

    let mut action: i32 = TC_ACT_OK;

    let mut new_addr: u32 = 0;
    let mut old_addr: u32 = 0;
    let new_addr6: u128;

    // Le validateur croit que `ctx` est modifié entre le première et deuxième
    // appelle de `make_forwarded_address6_key`, donc il faut appeler la fonction
    // une fois seulement
    let mut fwded_addr6_key: Option<ForwardedAddress6Key> = None;

    unsafe {
        let temps_s = ns_to_s(bpf_ktime_get_ns());

        // Chemin rapide pour les paquets de retour d'une connexion destiné au WAN.
        // `bpf_fib_lookup` retourne `BPF_FIB_LKUP_RET_NO_NEIGH` parfois pour les
        // recherches WAN -> LAN qui cause la perte des paquets, donc on stocke les
        // données de la recherche LAN -> WAN afin de les utiliser pour router les
        // paquets de retour sans consulter la table FIB
        if from_wan {
            if ip_version == IpVersion::V4 {
                let fwded_addr_key = make_forwarded_address4_key(ctx, &ipv4_hdr, true)?;

                if let Some(fwded_addr_val) = FORWARDED_ADDRESSES.get(&fwded_addr_key) {
                    new_addr = fwded_addr_val.src_addr;
                    old_addr = (*ipv4_hdr.unwrap()).dst_addr;
                    (*ipv4_hdr.unwrap()).dst_addr = new_addr;

                    decrementer_ttl_ip(ipv4_hdr.unwrap());

                    (*eth_hdr)
                        .src_addr
                        .clone_from_slice(&fwded_addr_val.dst_mac);
                    (*eth_hdr)
                        .dst_addr
                        .clone_from_slice(&fwded_addr_val.src_mac);
                    action = bpf_redirect(fwded_addr_val.entry_ifindex, 0) as i32;
                    if action != TC_ACT_REDIRECT {
                        return sortie_tc(data, data_end, ingress_ifindex, ip_proto, action);
                    }

                    remplacer_addr_ipv4(ctx, old_addr, new_addr, &ipv4_hdr, false)?;
                }
            } else {
                let fwded_addr_key = make_forwarded_address6_key(ctx, &ipv6_hdr, true)?;

                if let Some(fwded_addr_val) = FORWARDED_ADDRESSES6.get(&fwded_addr_key) {
                    new_addr6 = fwded_addr_val.src_addr;
                    (*ipv6_hdr.unwrap())
                        .dst_addr
                        .in6_u
                        .u6_addr8
                        .copy_from_slice(&new_addr6.to_le_bytes());

                    (*ipv6_hdr.unwrap()).hop_limit -= 1;

                    (*eth_hdr)
                        .src_addr
                        .clone_from_slice(&fwded_addr_val.dst_mac);
                    (*eth_hdr)
                        .dst_addr
                        .clone_from_slice(&fwded_addr_val.src_mac);
                    action = bpf_redirect(fwded_addr_val.entry_ifindex, 0) as i32;
                    if action != TC_ACT_REDIRECT {
                        return sortie_tc(data, data_end, ingress_ifindex, ip_proto, action);
                    }
                }
            }

            return sortie_tc(data, data_end, ingress_ifindex, ip_proto, action);
        }

        // Pour les paquets non-WAN, si aucune entrée n'existe déjà dans `FORWARDED_ADDRSSES`,
        // en insérer une
        let mac_vide: [u8; 6] = [0; 6];

        if ip_version == IpVersion::V4 {
            let fwded_addr_key = make_forwarded_address4_key(ctx, &ipv4_hdr, false)?;
            let fwded_addr_val = ForwardedAddressVal {
                src_addr: (*ipv4_hdr.unwrap()).src_addr,
                entry_ifindex: ingress_ifindex,
                src_mac: mac_vide,
                dst_mac: mac_vide,
            };
            let _ =
                FORWARDED_ADDRESSES.insert(&fwded_addr_key, &fwded_addr_val, BPF_NOEXIST as u64);

            old_addr = (*ipv4_hdr.unwrap()).src_addr;
        } else {
            fwded_addr6_key = Some(make_forwarded_address6_key(ctx, &ipv6_hdr, false)?);
            let mut fwded_addr_val: ForwardedAddress6Val = mem::zeroed();
            fwded_addr_val.entry_ifindex = ingress_ifindex;
            fwded_addr_val.src_addr =
                u128::from_le_bytes((*ipv6_hdr.unwrap()).src_addr.in6_u.u6_addr8);
            let _ = FORWARDED_ADDRESSES6.insert(
                &fwded_addr6_key.unwrap().clone(),
                &fwded_addr_val,
                BPF_NOEXIST as u64,
            );
        }
    }

    // SAFETY: Tous les valeurs données à `preparer_parametres_fib` sont valides
    let mut fib_params = unsafe { preparer_parametres_fib(ctx, &ip_version, &ipv4_hdr, &ipv6_hdr) };
    // SAFETY: Le validateur rejete les appels non sécuritaires
    let lookup_res = unsafe {
        bpf_fib_lookup(
            ctx.ctx(),
            &mut fib_params as *mut _,
            mem::size_of::<bpf_fib_lookup>() as i32,
            0,
        ) as u32
    };
    match lookup_res {
        BPF_FIB_LKUP_RET_SUCCESS => {
            // Indique que le paquet a passé par un autre routeur intermédiare
            // SAFETY: `eth_hdr` et `ipv4_hdr` pointe toujours vers un endroit valide
            unsafe {
                let mut orig_dst_mac: [u8; 6] = [0; 6];
                orig_dst_mac.copy_from_slice(&((*eth_hdr).dst_addr));

                let mut orig_src_mac: [u8; 6] = [0; 6];
                orig_src_mac.copy_from_slice(&((*eth_hdr).src_addr));

                (*eth_hdr).src_addr.clone_from_slice(&fib_params.smac);
                (*eth_hdr).dst_addr.clone_from_slice(&fib_params.dmac);
                action = bpf_redirect(fib_params.ifindex, 0) as i32;
                if action != TC_ACT_REDIRECT {
                    return Ok(action);
                }

                if ip_version == IpVersion::V4 {
                    decrementer_ttl_ip(ipv4_hdr.unwrap());

                    let fwded_addr_key = make_forwarded_address4_key(ctx, &ipv4_hdr, false)?;

                    if let Some(mut fwded_addr_val) =
                        FORWARDED_ADDRESSES.get(&fwded_addr_key).copied()
                    {
                        fwded_addr_val.src_mac.clone_from_slice(&orig_src_mac);
                        fwded_addr_val.dst_mac.clone_from_slice(&orig_dst_mac);
                        FORWARDED_ADDRESSES
                            .insert(&fwded_addr_key, &fwded_addr_val, BPF_ANY as u64)
                            .map_err(|_e| ())?;
                    }

                    if let Some(addr_locale) = ADDRS_LOCAUX.get(fib_params.ifindex) {
                        new_addr = addr_locale.addr;
                        remplacer_addr_ipv4(ctx, old_addr, new_addr, &ipv4_hdr, true)?;
                    }
                } else {
                    if let Some(fwded_addr_val) =
                        FORWARDED_ADDRESSES6.get_ptr_mut(&fwded_addr6_key.unwrap())
                    {
                        (*fwded_addr_val).src_mac.copy_from_slice(&orig_src_mac);
                        (*fwded_addr_val).dst_mac.copy_from_slice(&orig_dst_mac);
                    }

                    if let Some(addr_locale) = ADDRS6_LOCAUX.get(fib_params.ifindex) {
                        new_addr6 = addr_locale.addr;
                        (*ipv6_hdr.unwrap())
                            .src_addr
                            .in6_u
                            .u6_addr8
                            .copy_from_slice(&new_addr6.to_le_bytes());
                    }

                    (*ipv6_hdr.unwrap()).hop_limit -= 1;
                }
            }
        }
        // Le destination est injoignable ou pas permis
        BPF_FIB_LKUP_RET_BLACKHOLE | BPF_FIB_LKUP_RET_UNREACHABLE | BPF_FIB_LKUP_RET_PROHIBIT => {
            action = TC_ACT_SHOT;
        }
        // Le paquet ne devrait pas être transmis vers un autre interface
        _ => {}
    }

    // TODO: Validateur croit que ctx a été modifié et qu'il est maintenant
    // invalide ici
    sortie_tc(data, data_end, ingress_ifindex, ip_proto, action)
}

fn try_xdp_etherorange(ctx: &MyXdpContext) -> Result<u32, ()> {
    // SAFETY: `ptr_at_mut` pointe toujours vers un endroit de mémoire valide
    // lorsque `Ok()` est retourné
    let eth_hdr: *mut EthHdr = ptr_at_mut(ctx, 0)?;
    let ether_type: EtherType = unsafe { (*eth_hdr).ether_type };

    let ip_version: IpVersion;
    match ether_type {
        EtherType::Ipv4 => {
            ip_version = IpVersion::V4;
        }
        EtherType::Ipv6 => {
            ip_version = IpVersion::V6;
        }
        _ => {
            return Ok(xdp_action::XDP_PASS);
        }
    }

    let ip_proto: IpProto;

    let mut ipv4_hdr: Option<*mut Ipv4Hdr> = None;
    let mut ipv6_hdr: Option<*mut Ipv6Hdr> = None;
    // SAFETY: `ptr_at_mut` garantie que les pointeurs sont valides
    unsafe {
        if ip_version == IpVersion::V4 {
            ipv4_hdr = Some(ptr_at_mut(ctx, EthHdr::LEN)?);
            ip_proto = (*ipv4_hdr.unwrap()).proto;
        } else {
            ipv6_hdr = Some(ptr_at_mut(ctx, EthHdr::LEN)?);
            ip_proto = (*ipv6_hdr.unwrap()).next_hdr;
        }
    }

    let mut action: u32 = xdp_action::XDP_PASS;

    if cfg!(redirect = "false") {
        return sortie_xdp(ctx, ip_proto, action);
    }

    let mut fib_params = unsafe { preparer_parametres_fib(ctx, &ip_version, &ipv4_hdr, &ipv6_hdr) };
    // SAFETY: Le validateur rejete les appels non sécuritaires
    let lookup_res = unsafe {
        bpf_fib_lookup(
            ctx.ctx(),
            &mut fib_params as *mut _,
            mem::size_of::<bpf_fib_lookup>() as i32,
            0,
        ) as u32
    };
    match lookup_res {
        BPF_FIB_LKUP_RET_SUCCESS => {
            // Indique que le paquet a passé par un autre routeur intermédiare
            // SAFETY: `eth_hdr` et `ipv4_hdr` pointe toujours vers un endroit valide
            unsafe {
                if ip_version == IpVersion::V4 {
                    decrementer_ttl_ip(ipv4_hdr.unwrap());
                } else {
                    (*ipv6_hdr.unwrap()).hop_limit -= 1;
                }

                (*eth_hdr).src_addr.clone_from_slice(&fib_params.smac);
                (*eth_hdr).dst_addr.clone_from_slice(&fib_params.dmac);
                action = bpf_redirect(fib_params.ifindex, 0) as u32;
            }
        }
        // Le destination est injoignable ou pas permis
        BPF_FIB_LKUP_RET_BLACKHOLE | BPF_FIB_LKUP_RET_UNREACHABLE | BPF_FIB_LKUP_RET_PROHIBIT => {
            action = xdp_action::XDP_DROP;
        }
        // Le paquet ne devrait pas être transmis vers un autre interface
        _ => {}
    }

    sortie_xdp(ctx, ip_proto, action)
}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
