use crate::net::{IcmpType, Icmpv6Type, IpVersion};
use crate::packet_context::{ptr_at, PacketContext};
use aya_bpf::{macros::map, maps::LruHashMap};
use core::mem;
use network_types::{
    eth::EthHdr,
    icmp::{IcmpHdr, IcmpHdrEcho, ICMP_HDR_LEN},
    ip::{IpProto, Ipv4Hdr, Ipv6Hdr},
    tcp::TcpHdr,
    udp::UdpHdr,
};

#[repr(C)]
pub struct ForwardedAddressKey {
    /// L'adresse source
    pub src_addr: u32,
    /// L'adresse destination
    pub dst_addr: u32,
    /// L'ID de transport source. Pour UDP et TCP, c'est le port source.
    /// Pour les requêtes echo ICMP, c'est l'ID du message echo
    pub src_transport_id: u16,
    /// L'ID de transport destination. Pour UDP et TCP, c'est le port source.
    /// Pour les requêtes echo ICMP, c'est l'ID du message echo
    pub dst_transport_id: u16,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct ForwardedAddress6Key {
    /// L'adresse source
    pub src_addr: [u8; 16],
    /// L'adresse destination
    pub dst_addr: [u8; 16],
    pub padding: u32,
    /// L'ID de transport source. Pour UDP et TCP, c'est le port source.
    /// Pour les requêtes echo ICMP, c'est l'ID du message echo
    pub src_transport_id: u16,
    /// L'ID de transport destination. Pour UDP et TCP, c'est le port source.
    /// Pour les requêtes echo ICMP, c'est l'ID du message echo
    pub dst_transport_id: u16,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct ForwardedAddressVal {
    /// L'adresse source
    pub src_addr: u32,
    /// L'indice d'interface d'entrée pour le paquet
    pub entry_ifindex: u32,
    /// L'adresse MAC source
    pub src_mac: [u8; 6],
    /// L'adresse MAC destination
    pub dst_mac: [u8; 6],
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct ForwardedAddress6Val {
    /// L'adresse source
    pub src_addr: u128,
    /// L'indice d'interface d'entrée pour le paquet
    pub entry_ifindex: u32,
    /// L'adresse MAC source
    pub src_mac: [u8; 6],
    /// L'adresse MAC destination
    pub dst_mac: [u8; 6],
}

#[map]
pub static FORWARDED_ADDRESSES: LruHashMap<ForwardedAddressKey, ForwardedAddressVal> =
    LruHashMap::<ForwardedAddressKey, ForwardedAddressVal>::with_max_entries(100000, 0);

#[map]
pub static FORWARDED_ADDRESSES6: LruHashMap<ForwardedAddress6Key, ForwardedAddress6Val> =
    LruHashMap::<ForwardedAddress6Key, ForwardedAddress6Val>::with_max_entries(100000, 0);

/// Trouver les identifiants de transport pour un paquet. Pour UDP et TCP,
/// les ports sont utilisés. Dans le cas des messages ICMP, c'est l'ID d'un
/// requête Echo si applicable
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `ip_version` - La version IP
/// * `ip_proto` - Le protocole IP
/// * `l4_offset` - Le décalage des données L4 par rapport à `ctx`
///
/// # Safety
///
/// `ctx` doit faire reference au contexte d'un paquet valide
#[inline(always)]
unsafe fn get_transport_identifiers(
    ctx: &dyn PacketContext,
    ip_version: IpVersion,
    ip_proto: &IpProto,
    l4_offset: usize,
) -> Result<(u16, u16), ()> {
    return match ip_proto {
        IpProto::Udp => {
            let udp_hdr: *const UdpHdr = ptr_at(ctx, l4_offset)?;
            Ok(((*udp_hdr).source, (*udp_hdr).dest))
        }
        IpProto::Tcp => {
            let tcp_hdr: *const TcpHdr = ptr_at(ctx, l4_offset)?;
            Ok(((*tcp_hdr).source, (*tcp_hdr).dest))
        }
        IpProto::Icmp if ip_version == IpVersion::V4 => {
            get_icmp_identifier(ctx, IpVersion::V4, l4_offset)
        }
        IpProto::Ipv6Icmp if ip_version == IpVersion::V6 => {
            get_icmp_identifier(ctx, IpVersion::V6, l4_offset)
        }
        _ => Ok((0, 0)),
    };
}

/// Créer un objet `ForwardedAddressKey` à partir d'un paquet
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `ipv4_hdr` - L'entête IPv4
/// * `ingress` - Si le clé devrait être fait d'une perspective entrante
///
/// # Safety
///
/// `ipv4_hdr` doit être un pointeur valide
#[inline(always)]
pub unsafe fn make_forwarded_address4_key(
    ctx: &dyn PacketContext,
    ipv4_hdr: &Option<*mut Ipv4Hdr>,
    ingress: bool,
) -> Result<ForwardedAddressKey, ()> {
    let l4_offset: usize = EthHdr::LEN + mem::size_of::<Ipv4Hdr>();

    let (src_transport_id, dst_transport_id) =
        get_transport_identifiers(ctx, IpVersion::V4, &(*ipv4_hdr.unwrap()).proto, l4_offset)?;

    if ingress {
        Ok(ForwardedAddressKey {
            src_addr: 0,
            dst_addr: (*ipv4_hdr.unwrap()).src_addr,
            src_transport_id: dst_transport_id,
            dst_transport_id: src_transport_id,
        })
    } else {
        Ok(ForwardedAddressKey {
            src_addr: 0,
            dst_addr: (*ipv4_hdr.unwrap()).dst_addr,
            src_transport_id: src_transport_id,
            dst_transport_id: dst_transport_id,
        })
    }
}

/// Créer un objet `ForwardedAddress6Key` à partir d'un paquet
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `ipv6_hdr` - L'entête IPv6
/// * `ingress` - Si le clé devrait être fait d'une perspective entrante
///
/// # Safety
///
/// `ipv6_hdr` doit être un pointeur valide
#[inline(always)]
pub unsafe fn make_forwarded_address6_key(
    ctx: &dyn PacketContext,
    ipv6_hdr: &Option<*mut Ipv6Hdr>,
    ingress: bool,
) -> Result<ForwardedAddress6Key, ()> {
    let l4_offset: usize = EthHdr::LEN + mem::size_of::<Ipv6Hdr>();

    let addr6_vide: [u8; 16] = [0; 16];

    let (src_transport_id, dst_transport_id) = get_transport_identifiers(
        ctx,
        IpVersion::V6,
        &(*ipv6_hdr.unwrap()).next_hdr,
        l4_offset,
    )?;

    if ingress {
        let mut key = ForwardedAddress6Key {
            src_addr: addr6_vide,
            dst_addr: addr6_vide,
            src_transport_id: dst_transport_id,
            dst_transport_id: src_transport_id,
            padding: 0,
        };
        key.dst_addr
            .copy_from_slice(&(*ipv6_hdr.unwrap()).src_addr.in6_u.u6_addr8);
        Ok(key)
    } else {
        let mut key = ForwardedAddress6Key {
            src_addr: addr6_vide,
            dst_addr: addr6_vide,
            src_transport_id: src_transport_id,
            dst_transport_id: dst_transport_id,
            padding: 0,
        };

        key.dst_addr
            .copy_from_slice(&(*ipv6_hdr.unwrap()).dst_addr.in6_u.u6_addr8);
        Ok(key)
    }
}

/// Retourner l'identifiant de transport pour un paquet ICMP. Il s'agit du champ
/// `id` pour les messages `EchoRequest et EchoReply
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `ip_version` - La version IPv4
/// * `l4_offset` - Le décalage des données L4 par rapport à `ctx`
///
/// # Safety
///
/// `ctx` doit faire reference au contexte d'un paquet valide
#[inline(always)]
unsafe fn get_icmp_identifier(
    ctx: &dyn PacketContext,
    ip_version: IpVersion,
    l4_offset: usize,
) -> Result<(u16, u16), ()> {
    let icmp_hdr: *const IcmpHdr = ptr_at(ctx, l4_offset)?;
    return match ip_version {
        IpVersion::V4 => {
            let icmp_type = IcmpType::try_from_u8((*icmp_hdr).type_);
            if icmp_type.is_some() {
                let unwrapped_icmp_type = icmp_type.unwrap();
                if unwrapped_icmp_type == IcmpType::EchoRequest
                    || unwrapped_icmp_type == IcmpType::EchoReply
                {
                    let icmp_hdr_echo: *const IcmpHdrEcho = ptr_at(ctx, l4_offset + ICMP_HDR_LEN)?;
                    return Ok(((*icmp_hdr_echo).id, 0));
                }
            }
            Ok((0, 0))
        }
        IpVersion::V6 => {
            let icmp_type = Icmpv6Type::try_from_u8((*icmp_hdr).type_);
            if icmp_type.is_some() {
                let unwrapped_icmp_type = icmp_type.unwrap();
                if unwrapped_icmp_type == Icmpv6Type::EchoRequest
                    || unwrapped_icmp_type == Icmpv6Type::EchoReply
                {
                    let icmp_hdr_echo: *const IcmpHdrEcho = ptr_at(ctx, l4_offset + ICMP_HDR_LEN)?;
                    return Ok(((*icmp_hdr_echo).id, 0));
                }
            }
            Ok((0, 0))
        }
    };
}
