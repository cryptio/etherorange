use crate::net::{IP_CSUM_OFF, TCP_CSUM_OFF, UDP_CSUM_OFF};
use crate::packet_context::MyTcContext;
use aya_bpf::bindings::*;
use network_types::ip::{IpProto, Ipv4Hdr};

/// Combien des nanosecondes se trouvent dans une seconde
const NS_DANS_S: u64 = 1000000000;
/// Combien des octets une adresse IPv4 prend
const IPV4_ADDR_OCTETS: u32 = 4;

/// Convertir un horodatage en nanosecondes à secondes
///
/// # Arguments
///
/// * `ns` - Le temps en nanosecondes
///
/// # Examples
///
/// Retourner le temps actuel en secondes:
/// ```
/// let temps_actuel_s = ns_to_s(bpf_ktime_get_ns());
/// ```
#[inline(always)]
pub fn ns_to_s(ns: u64) -> u32 {
    (ns / NS_DANS_S) as u32
}

/// Adaptation de `ip_decrease_ttl` dans include/net/ip.h du noyau Linux.
/// Le checksum est mise à jour automatiquement.
///
/// # Arguments
///
/// * `ipv4_hdr` - L'entête IPv4 à modifier
///
/// # Notes
///
/// Parce qu'un appel à ce fonction pourrait modifier le tampon interne du
/// paquet, tous les garanties fait par le validateur sont oubliés et doivent
/// être refait afin de pouvoir accéder le paquet directement
///
/// # Safety
///
/// `ipv4_hdr` doit etre un pointeur valide
#[inline(always)]
pub unsafe fn decrementer_ttl_ip(ipv4_hdr: *mut Ipv4Hdr) {
    let mut check = unsafe { (*ipv4_hdr).check } as u32;
    check += u16::from_be(0x0100) as u32;
    unsafe {
        (*ipv4_hdr).check = (check + ((check >= 0xFFFF) as u32)) as u16;
        (*ipv4_hdr).ttl -= 1;
    }
}

/// Remplacer un adresse dans `ipv4_hdr` et met à jour le checksum
///
/// # Arguments
///
/// * `old_addr` - La veille adresse
/// * `new_addr` - La nouvelle adresse
/// * `ipv4_hdr` - L'entête IPv4
/// * `source`- Si c'est l'adresse source qui devrait être remplacé au lieu de
///             destination
///
/// # Safety
///
/// `ipv4_hdr` doit etre un pointeur valide
#[inline(always)]
pub unsafe fn remplacer_addr_ipv4(
    ctx: &MyTcContext,
    old_addr: u32,
    new_addr: u32,
    ipv4_hdr: &Option<*mut Ipv4Hdr>,
    source: bool,
) -> Result<(), ()> {
    if source {
        (*ipv4_hdr.unwrap()).src_addr = new_addr;
    } else {
        (*ipv4_hdr.unwrap()).dst_addr = new_addr;
    }

    let l4_csum_off: usize = match (*ipv4_hdr.unwrap()).proto {
        IpProto::Udp => UDP_CSUM_OFF,
        IpProto::Tcp => TCP_CSUM_OFF,
        _ => {
            return Ok(());
        }
    };

    ctx.0
        .skb
        .l3_csum_replace(
            IP_CSUM_OFF,
            old_addr as u64,
            new_addr as u64,
            IPV4_ADDR_OCTETS.into(),
        )
        .map_err(|_e| ())?;

    ctx.0
        .skb
        .l4_csum_replace(
            l4_csum_off,
            old_addr as u64,
            new_addr as u64,
            (BPF_F_PSEUDO_HDR | IPV4_ADDR_OCTETS) as u64,
        )
        .map_err(|_e| ())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_ns_to_s() {
        assert_eq!(ns_to_s(56788966576), 56);
    }

    #[test]
    fn test_decrementer_ttl_ip() {
        let ipv4_hdr = mem::zeroed::<Ipv4Hdr>();
        ipv4_hdr.ttl = 128;
        unsafe { test_decrementer_ttl_ip(ipv4_hdr) };
        assert_eq!(ipv4_hdr.ttl, 127);
    }
}
