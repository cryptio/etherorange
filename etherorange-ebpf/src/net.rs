use core::mem;
use network_types::{eth::EthHdr, ip::Ipv4Hdr};

pub const IP_CSUM_OFF: usize = EthHdr::LEN + 10;
pub const UDP_CSUM_OFF: usize = EthHdr::LEN + mem::size_of::<Ipv4Hdr>() + 6;
pub const TCP_CSUM_OFF: usize = EthHdr::LEN + mem::size_of::<Ipv4Hdr>() + 16;

/// Les versions IP possibles
#[repr(u8)]
#[derive(PartialEq)]
pub enum IpVersion {
    V4 = 4,
    V6 = 6,
}

/// Les familles d'adresse qui sont gérés
#[repr(u8)]
pub enum AddressFamily {
    /// IPv4
    AfInet = 2,
    /// IPv6
    AfInet6 = 10,
}

/// Les types des messages ICMP qui nous intéresse
#[repr(u8)]
#[derive(PartialEq, Debug)]
pub enum IcmpType {
    EchoReply = 0,
    EchoRequest = 8,
}

/// Les types des messages ICMPv6 qui nous intéresse
#[repr(u8)]
#[derive(PartialEq, Debug)]
pub enum Icmpv6Type {
    EchoRequest = 128,
    EchoReply = 129,
}

impl IcmpType {
    /// Essayer de retourner un `IcmpType` à partir d'un `u8`
    ///
    /// # Arguments
    ///
    /// * `value` - Le valeur à convertir
    #[inline(always)]
    pub fn try_from_u8(value: u8) -> Option<IcmpType> {
        match value {
            0 => Some(IcmpType::EchoReply),
            8 => Some(IcmpType::EchoRequest),
            _ => None,
        }
    }
}

impl Icmpv6Type {
    /// Essayer de retourner un `Icmpv6Type` à partir d'un `u8`
    ///
    /// # Arguments
    ///
    /// * `value` - Le valeur à convertir
    #[inline(always)]
    pub fn try_from_u8(value: u8) -> Option<Icmpv6Type> {
        match value {
            128 => Some(Icmpv6Type::EchoRequest),
            129 => Some(Icmpv6Type::EchoReply),
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_icmp_type_from_u8() {
        assert_eq!(IcmpType::try_from_u8(0), Some(IcmpType::EchoRequest));
        assert_eq!(IcmpType::try_from_u8(8), Some(IcmpType::EchoReply));
        assert!(IcmpType::try_from_u8(u8::MAX).is_none())
    }

    #[test]
    fn test_icmp_v6_type_from_u8() {
        assert_eq!(Icmpv6Type::try_from_u8(128), Some(Icmpv6Type::EchoRequest));
        assert_eq!(Icmpv6Type::try_from_u8(129), Some(Icmpv6Type::EchoReply));
        assert!(Icmpv6Type::try_from_u8(u8::MAX).is_none())
    }
}
