use aya_bpf::{
    programs::{TcContext, XdpContext},
    BpfContext,
};
use core::{ffi::c_void, mem};

/// Interface générique qui nous permet de traiter les paquets dans XDP et TC dans
/// la même façon
pub trait PacketContext {
    /// Retourner le décalage du début des données
    fn data(&self) -> usize;

    /// Retourner le décalage du fin des données
    fn data_end(&self) -> usize;

    /// Retourner un pointer vers la contexte qui est stocké à l'interne.
    /// Par exemple, pour les programmes TC, il s'agit d'un `__sk_buff` lorsqu'il
    /// fait référence à `xdp_md` pour les programmes XDP
    fn ctx(&self) -> *mut c_void;

    /// Retourner l'indice d'interface pour ce paquet
    ///
    /// # Safety
    ///
    /// Le pointeur de contexte qui est stocké à l'interne doit être valide
    unsafe fn ingress_ifindex(&self) -> u32;

    /// Retourner l'indice d'interface pour ce paquet
    ///
    /// # Safety
    ///
    /// Le pointeur de contexte qui est stocké à l'interne doit être valide
    unsafe fn ifindex(&self) -> u32;
}

/// MyXdpContext implémente `PacketContext` pour `XdpContext`
pub struct MyXdpContext(pub XdpContext);

/// MyTcContext implémente `PacketContext` pour `TcContext`
pub struct MyTcContext(pub TcContext);

impl PacketContext for MyXdpContext {
    fn data(&self) -> usize {
        self.0.data()
    }

    fn data_end(&self) -> usize {
        self.0.data_end()
    }

    fn ctx(&self) -> *mut c_void {
        self.0.ctx as *mut c_void
    }

    /// Retourne l'indice d'interface d'entrée
    ///
    /// # Safety
    ///
    /// `ctx` ne peut pas être nulle
    unsafe fn ingress_ifindex(&self) -> u32 {
        (*self.0.ctx).ingress_ifindex
    }

    /// Retourner l'indice d'interface pour ce paquet.
    ///
    /// Toujours 0 parce que les programmes XDP ne peuvent êtres exécutés que
    /// sur les paquets de sortie
    unsafe fn ifindex(&self) -> u32 {
        0
    }
}

impl BpfContext for MyXdpContext {
    fn as_ptr(&self) -> *mut c_void {
        self.ctx()
    }
}

impl PacketContext for MyTcContext {
    fn data(&self) -> usize {
        self.0.data()
    }

    fn data_end(&self) -> usize {
        self.0.data_end()
    }

    fn ctx(&self) -> *mut c_void {
        self.0.skb.skb as *mut _
    }

    unsafe fn ingress_ifindex(&self) -> u32 {
        (*self.0.skb.skb).ingress_ifindex
    }

    unsafe fn ifindex(&self) -> u32 {
        (*self.0.skb.skb).ifindex
    }
}

impl BpfContext for MyTcContext {
    fn as_ptr(&self) -> *mut c_void {
        self.ctx()
    }
}

/// Essayer de retourner un pointeur mutable vers le paquet au décalage spécifié.
///
/// Le fonction échoue si l'emplacement demandé dépasse le fin du paquet
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `offset` - La décalage du `ctx` à utiliser pour chercher un pointeur
///
/// # Examples
///
/// Chercher un pointeur mutable vers l'entête IPv4:
/// ```
/// let mut ipv4_hdr = ptr_at_mut(ctx, EthHdr::LEN)?;
/// ```
#[inline(always)]
pub fn ptr_at_mut<T>(ctx: &dyn PacketContext, offset: usize) -> Result<*mut T, ()> {
    let start = ctx.data();
    let end = ctx.data_end();
    let len = mem::size_of::<T>();

    if start + offset + len > end {
        return Err(());
    }

    Ok((start + offset) as *mut T)
}

/// Essayer de retourner un pointeur vers le paquet au décalage spécifié.
///
/// Le fonction échoue si l'emplacement demandé dépasse le fin du paquet
///
/// # Arguments
///
/// * `ctx` - La contexte du paquet
/// * `offset` - La décalage du `ctx` à utiliser pour chercher un pointeur
///
/// # Examples
///
/// Chercher un pointeur vers l'entête IPv4:
/// ```
/// let ipv4_hdr = ptr_at(ctx, EthHdr::LEN)?;
/// ```
#[inline(always)]
pub fn ptr_at<T>(ctx: &dyn PacketContext, offset: usize) -> Result<*const T, ()> {
    Ok(ptr_at_mut(ctx, offset)? as *const T)
}
