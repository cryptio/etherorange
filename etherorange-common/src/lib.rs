#![no_std]
use network_types::ip::IpProto;

#[repr(C)]
#[derive(Clone, Copy)]
pub struct AddrLocale {
    /// L'adresse IP associée à l'interface
    pub addr: u32,
    /// Si c'est l'adresse LAN du système
    pub lan: bool,
    pub padding: [u8; 3],
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Addr6Locale {
    /// L'adresse IP associée à l'interface
    pub addr: u128,
    pub padding: [u8; 7],
    /// Si c'est l'adresse LAN du système
    pub lan: bool,
}

#[cfg(feature = "user")]
unsafe impl aya::Pod for AddrLocale {}
#[cfg(feature = "user")]
unsafe impl aya::Pod for Addr6Locale {}

/// Clé pour le map `COMPTEURS`
#[repr(C)]
#[derive(Clone, Copy)]
pub struct CompteurKey {
    /// L'indice d'interface
    pub ifindex: u32,
    /// Le protocole IP
    pub proto: IpProto,
    pub padding: [u8; 3],
}

/// Représente le valeur du map `COMPTEURS`
#[repr(C)]
#[derive(Clone, Copy)]
pub struct CompteurVal {
    /// Combien des octets jusqu'à date
    pub octets: u64,
    /// Combien des paquets jusqu'à date
    pub paquets: u64,
}

#[cfg(feature = "user")]
unsafe impl aya::Pod for CompteurKey {}
#[cfg(feature = "user")]
unsafe impl aya::Pod for CompteurVal {}
