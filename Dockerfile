FROM rustlang/rust:nightly-bullseye as builder

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && wget https://apt.llvm.org/llvm-snapshot.gpg.key \
    && apt-key add llvm-snapshot.gpg.key \
    && rm -f llvm-snapshot.gpg.key \
    && add-apt-repository "deb http://apt.llvm.org/bullseye/ llvm-toolchain-bullseye-15 main" \
    && apt-get update \
    && apt-get install -y \
    libssl-dev \
    llvm-15-dev \
    musl \
    musl-dev \
    musl-tools \
    pkg-config \
    libclang-common-11-dev \
    libpolly-15-dev

RUN rustup component add rust-src
RUN rustup target add aarch64-unknown-linux-musl
RUN cargo install --no-default-features --features system-llvm bpf-linker
COPY . /src
WORKDIR /src
RUN --mount=type=cache,target=/.root/cargo/registry \
    --mount=type=cache,target=/src/target \
    cargo xtask build-ebpf --release \
    && cargo build --release --target=aarch64-unknown-linux-musl \
    && cp /src/target/aarch64-unknown-linux-musl/release/etherorange /usr/sbin

FROM alpine:latest
# runc links those libraries dynamically
RUN apk update \
    && apk add libseccomp \
    libselinux
COPY --from=builder /usr/sbin/etherorange /usr/sbin/
ENTRYPOINT [ "/usr/sbin/etherorange" ]
