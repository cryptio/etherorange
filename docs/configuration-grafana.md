# Tableau de bord Grafana

## Prometheus
Prometheus communique avec le serveur de statistiques pour chercher les données. Faire référence au [guide d'installation](https://grafana.com/docs/grafana/latest/getting-started/get-started-grafana-prometheus/) pour configurer Prometheus. Dans le fichier de configuration, la section
```yml
       static_configs:
       - targets: ['localhost:9100']
```
doit être lié à l'adresse de votre serveur de statistiques (port 8080 par défaut).


## Grafana
Consulter le [guide d'installation](https://grafana.com/docs/grafana/latest/setup-grafana/installation/) de Grafana pour l'installer.
Ensuite, ajouter un source de données de type Prometheus. L'URI doit être l'adresse du client Prometheus. Par défaut, c'est port 9090.
![](Grafana%20Data%20Source.png)


### Panneaux Grafana

Une fois que Grafana et Prometheus ont été configuré, vous pouvez créer un tableau de bord Grafana pour voir les données. Voici quelques exemples des panneaux qui peuvent être utilisés pour visualiser le trafic réseau.

![Panels](./Panneaux.png)

Paquets (LAN):
```json
{
  "datasource": {
    "type": "prometheus",
    "uid": "TTr978EVk"
  },
  "fieldConfig": {
    "defaults": {
      "custom": {
        "drawStyle": "line",
        "lineInterpolation": "linear",
        "barAlignment": 0,
        "lineWidth": 1,
        "fillOpacity": 0,
        "gradientMode": "none",
        "spanNulls": false,
        "showPoints": "auto",
        "pointSize": 5,
        "stacking": {
          "mode": "none",
          "group": "A"
        },
        "axisPlacement": "auto",
        "axisLabel": "",
        "axisColorMode": "text",
        "scaleDistribution": {
          "type": "linear"
        },
        "axisCenteredZero": false,
        "hideFrom": {
          "tooltip": false,
          "viz": false,
          "legend": false
        },
        "thresholdsStyle": {
          "mode": "off"
        }
      },
      "color": {
        "mode": "palette-classic"
      },
      "mappings": [],
      "thresholds": {
        "mode": "absolute",
        "steps": [
          {
            "color": "green",
            "value": null
          },
          {
            "color": "red",
            "value": 80
          }
        ]
      },
      "unit": "pps"
    },
    "overrides": []
  },
  "gridPos": {
    "h": 9,
    "w": 12,
    "x": 0,
    "y": 0
  },
  "id": 2,
  "options": {
    "tooltip": {
      "mode": "single",
      "sort": "none"
    },
    "legend": {
      "showLegend": true,
      "displayMode": "list",
      "placement": "bottom",
      "calcs": []
    }
  },
  "targets": [
    {
      "datasource": {
        "type": "prometheus",
        "uid": "TTr978EVk"
      },
      "editorMode": "code",
      "expr": "sum by (protocol) (rate(paquets{interface=\"lan0\"}[$__interval]))",
      "legendFormat": "__auto",
      "range": true,
      "refId": "A"
    }
  ],
  "title": "Paquets (LAN)",
  "transformations": [
    {
      "id": "groupBy",
      "options": {}
    }
  ],
  "type": "timeseries"
}
```

Bits (LAN):
```json
{
  "datasource": {
    "type": "prometheus",
    "uid": "TTr978EVk"
  },
  "fieldConfig": {
    "defaults": {
      "custom": {
        "drawStyle": "line",
        "lineInterpolation": "linear",
        "barAlignment": 0,
        "lineWidth": 1,
        "fillOpacity": 0,
        "gradientMode": "none",
        "spanNulls": false,
        "showPoints": "auto",
        "pointSize": 5,
        "stacking": {
          "mode": "none",
          "group": "A"
        },
        "axisPlacement": "auto",
        "axisLabel": "",
        "axisColorMode": "text",
        "scaleDistribution": {
          "type": "linear"
        },
        "axisCenteredZero": false,
        "hideFrom": {
          "tooltip": false,
          "viz": false,
          "legend": false
        },
        "thresholdsStyle": {
          "mode": "off"
        }
      },
      "color": {
        "mode": "palette-classic"
      },
      "mappings": [],
      "thresholds": {
        "mode": "absolute",
        "steps": [
          {
            "color": "green",
            "value": null
          },
          {
            "color": "red",
            "value": 80
          }
        ]
      },
      "unit": "binbps"
    },
    "overrides": []
  },
  "gridPos": {
    "h": 9,
    "w": 12,
    "x": 0,
    "y": 9
  },
  "id": 5,
  "options": {
    "tooltip": {
      "mode": "single",
      "sort": "none"
    },
    "legend": {
      "showLegend": true,
      "displayMode": "list",
      "placement": "bottom",
      "calcs": []
    }
  },
  "targets": [
    {
      "datasource": {
        "type": "prometheus",
        "uid": "TTr978EVk"
      },
      "editorMode": "code",
      "expr": "sum by (protocol) (rate(octets{interface=\"lan0\"}[$__interval])) * 8",
      "legendFormat": "__auto",
      "range": true,
      "refId": "A"
    }
  ],
  "title": "Bits (LAN)",
  "transformations": [
    {
      "id": "groupBy",
      "options": {}
    }
  ],
  "type": "timeseries"
}
```
