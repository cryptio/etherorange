# etherorange

## Description
etherorange s'agit d'un logiciel qui permet de router des paquets sur un
système sans l'aide des pare-feux traditionnels comme iptables. Vous devriez être en mesure de l'utiliser sur n'importe quelle machine, mais le projet était fait sous les conditions suivantes:
- Système : OrangePi R1 Plus LTS
- Architecture : ARM64
- Système d'exploitation : Armbian Debian Buster
- Noyau : 5.10.44-rockchip64

## Fonctionnalités
- Collection des statistiques - l'application peut collecter des statistiques sur tout le trafic qui passe par l'application. Si vous placez etherorange entre un réseau LAN et WAN (eg. entre votre modem et vos appareils LAN), vous pouvez avoir un aperçu de tout le trafic sur votre réseau
- Visualisation des données - etherorange expose ses statistiques avec un serveur Prometheus. Pour avoir un panneau web qui permet de visualiser les statistiques du trafic géré par etherorange, voir [Tableau de bord Grafana](./docs/configuration-grafana.md)
- Network Address Translation (NAT) - en tant que système avec deux interfaces, chacun ayant sont propre adresse IP sur un réseau diffèrent, etherorange est capable de traduire l'adresse IP pour chaque côté.
- Routage à haut vitesse avec `XDP_REDIRECT` - pour les systèmes qui le soutient (seulement certains pilotes l'on implémenté), le composant XDP de etherorange peut router des paquets entre deux interfaces sans besoin de passer par la majorité du pile TCP/IP Linux


## Dépendances
- Au moins deux interfaces Ethernet - l'un pour le trafic LAN, et l'autre pour le trafic WAN. Idéalement, il va y avoir un carte sans-fil qui peut être utilisé pour la connexion Prometheus <-> etherorange.
- Système d'exploitation - N'importe quelle distribution Linux
- Mémoire - 1 Go
- Stockage - 32 Go



## Structure
```
├───docs - Dossier de documentation
├───etherorange - Le programme user-space qui charge le programme eBPF et lit les statistiques
├───etherorange-common - Définition des types qui sont partagés entre le code kernel-space et user-space
├───etherorange-ebpf - Le progrramme eBPF qui s'attache aux interfaces réseau
├───xtask - Dossier généré par aya qui sert à faciliter la compilation et lancement du projet
```


## Compilation
### Manuellement

#### Dependances
1. Installer la chaîne d'outils pour Rust stable: `rustup install stable`
2. Installer la chaîne d'outils pour Rust nightly: `rustup install nightly`
3. Installer bpf-linker:

    Pour Linux x86_64: `cargo install bpf-linker`
    
    Pour MacOS ou Linux sur les autres architectures:

    `cargo install --no-default-features --features bpf-linker`

#### Compile eBPF

```bash
cargo xtask build-ebpf
```

Pour compiler en mode release, il suffit de passer l'argument `--release`.
L'architecture peut être contrôlé avec l'argument `--target`.

#### Compile Userspace

```bash
cargo build
```

### Docker
```bash
sudo docker build -t etherorange -f ./Dockerfile .
```

## Exécution
Remplacer `...` avec vos arguments.

### Régulièrement
```bash
RUST_LOG=info cargo xtask run -- ...
```

### Docker
```bash
sudo docker run -it -e RUST_LOG=info --network host --ulimit memlock=1073741824 --privileged -v /sys/:/sys/ etherorange ...
 ```

## Méthodes d'utilisation
### Deux interfaces joint par un pont
Utiliser l'argument `-i` pour spécifier l'interface.

### Interfaces séparées
Utiliser les arguments `--lan-iface`, `--wan-iface`. `--utilise-nat` doit être présent également.
